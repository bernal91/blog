import {Component} from '@angular/core';
import * as firebase from 'firebase';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  constructor() {

    const firebaseConfig = {
      apiKey: "AIzaSyDbbIo0pqF_rlQR0nhfwLFbqMsao8sLd9M",
      authDomain: "posts-dbed2.firebaseapp.com",
      databaseURL: "https://posts-dbed2.firebaseio.com",
      projectId: "posts-dbed2",
      storageBucket: "posts-dbed2.appspot.com",
      messagingSenderId: "196320000841",
      appId: "1:196320000841:web:2f49fb853ff3008c8ff898"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
