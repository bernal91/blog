import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {PostService} from "../services/post.service";
import {Post} from '../models/post-model';
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.less']
})
export class NewPostComponent implements OnInit {

  postForm: FormGroup;
  fileUrl: string;
  fileIsUploading = false;
  fileUploaded = false;

  constructor(private formBuilder: FormBuilder, private postService: PostService, private router: Router) {
  }

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      content: ['', Validators.required],
    });
  }


  savePost() {
    const title = this.postForm.get('title').value;
    const author = this.postForm.get('author').value;
    const content = this.postForm.get('content').value;

    let newPost = new Post(title, author, content);

    if (this.fileUrl && this.fileUrl !== '') {
      newPost.photo = this.fileUrl;
    }


    this.postService.createNewPost(newPost);
    this.router.navigate(['/postList'])
  }

  detectFiles(event) {
    console.log(event);
    this.onUploadFile(event.target.files[0]);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.postService.uploadFile(file).then(
      (url: string) => {
        console.log('url', url);
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    )
  }

}
