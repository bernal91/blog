import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.less']
})
export class CreateUserComponent implements OnInit {

  signupForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      mail: ['', [Validators.required, Validators.email]],
      psw: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    const mail = this.signupForm.get('mail').value;
    const psw = this.signupForm.get('psw').value;

    this.authService.createUser(mail, psw).then(
      () => {
        this.router.navigate(['/postList']);
      },
      err => {
        this.errorMessage = err;
      }
    )
  }
}
