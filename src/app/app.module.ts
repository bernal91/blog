import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderViewComponent} from './header-view/header-view.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import {PostListComponent} from './post-list/post-list.component';
import {PostListViewComponent} from './post-list-view/post-list-view.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NewPostComponent} from './new-post/new-post.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {PostService} from "./services/post.service";
import {AuthService} from "./services/auth.service";
import {MatCardModule, MatInputModule} from "@angular/material";
import {MatButtonModule} from "@angular/material";
import {MatFileUploadModule} from 'angular-material-fileupload';
import {NgxMasonryModule} from 'ngx-masonry';
import {CreateUserComponent} from './create-user/create-user.component';
import {SigninComponent} from './signin/signin.component';
import {AuthGuardService} from "./services/auth-guard.service";

const appRoutes: Routes = [
  {path: 'newPost', canActivate: [AuthGuardService], component: NewPostComponent},
  {path: 'postList', canActivate: [AuthGuardService], component: PostListViewComponent},
  {path: 'signup', component: CreateUserComponent},
  {path: 'signin', component: SigninComponent},
  {path: '**', component: SigninComponent, pathMatch: 'full'},
  {path: '', component: SigninComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderViewComponent,
    PostListComponent,
    PostListViewComponent,
    NewPostComponent,
    CreateUserComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatFileUploadModule,
    NgxMasonryModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostService, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {
  private static authService: any;
}
