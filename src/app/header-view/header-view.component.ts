import { Component, OnInit } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {AuthService} from "../services/auth.service";
import * as firebase from "firebase";

@Component({
  selector: 'app-header-view',
  templateUrl: './header-view.component.html',
  styleUrls: ['./header-view.component.less']
})
export class HeaderViewComponent implements OnInit {

  isAuth: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(
      user=>{
        user ? this.isAuth = true : this.isAuth = false;
      }
    )

  }


  logOut(){
    this.authService.onLogOut();
  }

}
