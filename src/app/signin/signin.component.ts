import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.less']
})
export class SigninComponent implements OnInit {

  signinForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.signinForm = this.formBuilder.group({
      mail: ['', [Validators.required, Validators.email]],
      psw: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    })
  }

  onSubmit(){
    const mail = this.signinForm.get('mail').value;
    const psw = this.signinForm.get('psw').value;

    this.authService.onSignIn(mail, psw).then(
      ()=>{
        this.router.navigate(['/postList']);
      },
      err=>{
        this.errorMessage = err;
      }
    )

  }
}
