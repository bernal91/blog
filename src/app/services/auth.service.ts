import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {promise} from "selenium-webdriver";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  constructor(private router: Router) {

  }

  createUser(mail: string, psw: string) {

    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(mail, psw).then(() => {
            resolve();
          },
          err => {
            reject(err)
          }
        )
      }
    )
  }

  //connexion
  onSignIn(mail: string, psw: string) {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(mail, psw).then(
          () => {
            resolve();
          },
          err => {
            reject(err);
          }
        )
      }
    )
  }

  onLogOut() {
    firebase.auth().signOut();
    this.router.navigate(['/signin'])
  }

}
